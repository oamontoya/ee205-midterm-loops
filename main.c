///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Midterm - Loops
///
/// @file    main.c
/// @version 1.0
///
/// Sums up values in arrays
///
/// Your task is to print out the sums of the 3 arrays...
///
/// The three arrays are held in a structure.  Consult numbers.h for the details.
///
/// For array1[], you'll iterate over using a for() loop.  The correct sum for 
/// array1[] is:  48723737032
/// 
/// For array2[], you'll iterate over it using a while()... loop.
///
/// For array3[], you'll iterate over it using a do ... while() loop.
///
/// Sample output:  
/// $ ./main
/// 11111111111
/// 22222222222
/// 33333333333
/// $
///
/// @brief Midterm - Loops - EE 205 - Spr 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "numbers.h"

int main() {
   // Sum array1[] with a for() loop and print the result
   unsigned long long sum1 = 0;
   for(int i = 0; i < 100; i++)
      {
         sum1 = sum1 + threeArrays.array1[i];
      }
   printf("%llu\n", sum1);

   // Sum array2[] with a while() { } loop and print the result
   unsigned long long sum2 = 0;
   int counter = 0;
   while(counter < 100){
   sum2 += threeArrays.array2[counter];
      counter++;
   }
   printf("%llu\n", sum2);
   // Sum array3[] with a do { } while() loop and print the result
   int index = 0;
   long sum3 = 0;
   do{
      sum3 = sum3 + threeArrays.array3[index++];
   }while(index <= 100);
   
   printf("%1lu\n", sum3);

	return 0;
}

